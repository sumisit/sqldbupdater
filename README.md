# SqlDbUpdater #

A simple tool to manage database versions.

This tool is inspired by the blog series written by K. Scott Allen:

1. [Three Rules for Database Work](http://odetocode.com/blogs/scott/archive/2008/01/30/three-rules-for-database-work.aspx)
1. [The Baseline](http://odetocode.com/Blogs/scott/archive/2008/01/31/11710.aspx)
1. [Change Scripts](http://odetocode.com/Blogs/scott/archive/2008/02/02/11721.aspx)
1. [Views, Stored Procedures and the Like](http://odetocode.com/Blogs/scott/archive/2008/02/02/11737.aspx)
1. [Branching and Merging](http://odetocode.com/Blogs/scott/archive/2008/02/03/11746.aspx)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact