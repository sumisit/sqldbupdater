﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;

namespace SqlDbUpdate
{
    public class Updater
    {
        private static readonly Regex GoRegex = new Regex(@"(^|\s)GO(?=\s|$)", RegexOptions.IgnoreCase);

        private static readonly ILog Log = LogManager.GetLogger(typeof(Updater));

        private const string ChangesFolder = "Changes";
        private const string ObjectsFolder = "Objects";

        private readonly string connectionString;
        private readonly string folder;

        public Updater(string connectionString, string folder)
        {
            this.connectionString = connectionString;
            this.folder = folder;
        }

        public void Execute()
        {
            if (!string.IsNullOrEmpty(folder))
            {
                if (!Directory.Exists(folder))
                {
                    Log.WarnFormat("Main script directory not found {0} - no scripts to run. Will exit.", folder);
                    return;
                }

                Environment.CurrentDirectory = folder;

                if (!Directory.Exists(ChangesFolder))
                    Log.Warn(@"Changes script folder not found. Looking for folder Scripts\Changes");

                if (!Directory.Exists(ObjectsFolder))
                    Log.Warn(@"Objects script folder not found. Looking for folder Scripts\Objects");
            }

            SortedSet<ChangeScriptFile> changeScriptFiles = null;

            if (Directory.Exists(ChangesFolder))
            {
                changeScriptFiles = new SortedSet<ChangeScriptFile>(Directory.GetFiles(ChangesFolder, "*.sql").Select(filePath => new ChangeScriptFile(Path.GetFileName(filePath), Checksum.FromFile(filePath))), new ChangeScriptFileNameComparer());
            }

            using (var connection = new SqlConnection(connectionString))
            {
                Log.Info("Opening connection to database...");
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    Log.Info("Starting transaction...");
                    try
                    {
                        using (var checkTableCmd = connection.CreateCommand())
                        {
                            checkTableCmd.Transaction = transaction;
                            checkTableCmd.CommandText = @"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChangeScripts]') AND type in (N'U'))
															BEGIN
															CREATE TABLE ChangeScripts(
																ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_ChangeScripts PRIMARY KEY CLUSTERED,
																ScriptFileName nvarchar(255) NOT NULL,
																RunTimestamp datetimeoffset(0) NOT NULL,
																Checksum binary(16) NOT NULL,
																Skipped bit NOT NULL,
																CONSTRAINT UK_ChangeScripts_CheckSum_ScriptFileName UNIQUE NONCLUSTERED (Checksum ASC, ScriptFileName ASC)
															)END";
                            Log.Info("Creating ChangeScript table if not exists...");
                            checkTableCmd.ExecuteNonQuery();
                        }

                        if (changeScriptFiles != null)
                        {
                            var runChangeFiles = new List<ChangeScriptFile>();
                            using (var changeFilesRunCmd = connection.CreateCommand())
                            {
                                changeFilesRunCmd.Transaction = transaction;
                                changeFilesRunCmd.CommandText = "SELECT ScriptFileName, RunTimestamp, Checksum FROM ChangeScripts";
                                using (var reader = changeFilesRunCmd.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        var checkSumBytes = new byte[16];
                                        reader.GetBytes(2, 0, checkSumBytes, 0, 16);
                                        runChangeFiles.Add(new ChangeScriptFile(reader.GetString(0), new Checksum(checkSumBytes), reader.GetDateTimeOffset(1)));
                                    }
                                }
                            }

                            var changeScriptFilesToRun = new List<ChangeScriptFile>();
                            var changeScriptFilesToSkip = new List<ChangeScriptFile>();
                            foreach (var changeScriptFile in changeScriptFiles)
                            {
                                if (runChangeFiles.Contains(changeScriptFile))
                                {
                                    Log.DebugFormat("Skipping {0} because it already has been run.", changeScriptFile.FileName);
                                    continue;
                                }

                                ChangeScriptFile fileAlreadyRun = runChangeFiles.FirstOrDefault(sf => sf.FileName.Equals(changeScriptFile.FileName, StringComparison.OrdinalIgnoreCase));

                                if (fileAlreadyRun != null)
                                {
                                    Log.WarnFormat("A different version of file {0} has already been run on {1}.", changeScriptFile.FileName, fileAlreadyRun.RunTimestamp.Value.LocalDateTime);
                                    HandleConflict();

                                    continue;
                                }
                                Log.InfoFormat("Will run new file {0}.", changeScriptFile.FileName);
                                changeScriptFilesToRun.Add(changeScriptFile);
                            }

                            if (changeScriptFilesToRun.Count == 0)
                            {
                                Log.Info("No change scripts found that have not been run on the database.");
                            }
                            else
                            {
                                foreach (var changesFile in changeScriptFilesToRun)
                                {
                                    RunSqlFile(Path.Combine(ChangesFolder, changesFile.FileName), connection, transaction);
                                    InsertChangesFile(connection, transaction, changesFile, false);
                                }
                            }

                            foreach (var changeScriptFile in changeScriptFilesToSkip)
                            {
                                InsertChangesFile(connection, transaction, changeScriptFile, true);
                            }
                        }
                        if (Directory.Exists(ObjectsFolder))
                        {
                            foreach (var file in Directory.GetFiles(ObjectsFolder, "*.sql", SearchOption.AllDirectories))
                            {
                                RunSqlFile(file, connection, transaction);
                            }
                        }

                        Log.Info("Committing transaction..");
                        transaction.Commit();
                        Log.Info("All script have run successfully.");
                    }
                    catch (AbortException ex)
                    {
                        Rollback(transaction);
                        Log.Info("Rolled back transaction because the migration was aborted.", ex);
                        throw;
                    }
                    catch (Exception ex)
                    {
                        Rollback(transaction);
                        Log.Info("ERROR: " + ex.Message, ex);
                        throw;
                    }
                }
            }
        }

        private static void Rollback(SqlTransaction transaction)
        {
            Log.Info("Rolling back transaction...");
            transaction.Rollback();
        }

        private static void HandleConflict()
        {
            Log.Info("Aborting....");
            throw new AbortException();
        }

        private static void InsertChangesFile(SqlConnection connection, SqlTransaction transaction, ChangeScriptFile changesFile, bool skipped)
        {
            using (var insertChangeScriptCmd = connection.CreateCommand())
            {
                insertChangeScriptCmd.Transaction = transaction;
                insertChangeScriptCmd.CommandText = "INSERT INTO ChangeScripts (ScriptFileName, RunTimestamp, Checksum, Skipped) VALUES (@scriptFileName, @runTimestap, @checksum, @skipped)";
                insertChangeScriptCmd.Parameters.Add("scriptFileName", SqlDbType.NVarChar, 255).Value = changesFile.FileName;
                insertChangeScriptCmd.Parameters.Add("runTimestap", SqlDbType.DateTimeOffset, 4).Value = DateTimeOffset.Now;
                insertChangeScriptCmd.Parameters.Add("checksum", SqlDbType.Binary, 16).Value = changesFile.Checksum.ToByteArray();
                insertChangeScriptCmd.Parameters.Add("skipped", SqlDbType.Bit).Value = skipped;
                insertChangeScriptCmd.ExecuteNonQuery();
            }
        }

        private static void RunSqlFile(string changesFile, SqlConnection connection, SqlTransaction transaction)
        {
            Log.InfoFormat("Running {0}....", changesFile);
            string sqlTxt = File.ReadAllText(changesFile);
            MatchCollection goMatches = GoRegex.Matches(sqlTxt);
            int index = 0;
            foreach (Match goMatch in goMatches)
            {
                string sqlBlock = sqlTxt.Substring(index, goMatch.Index - index);
                ExecuteSql(sqlBlock, connection, transaction);
                index = goMatch.Index + goMatch.Length;
            }
            if (index < sqlTxt.Length)
            {
                ExecuteSql(sqlTxt.Substring(index), connection, transaction);
            }
        }

        private static void ExecuteSql(string sqlBlock, SqlConnection connection, SqlTransaction transaction)
        {
            if (string.IsNullOrWhiteSpace(sqlBlock))
            {
                return;
            }
            using (var cmd = connection.CreateCommand())
            {
                Log.Debug(sqlBlock);
                cmd.Transaction = transaction;
                cmd.CommandText = sqlBlock;
                cmd.ExecuteNonQuery();
            }
        }
    }
}