﻿using System;
using System.Collections.Generic;

namespace SqlDbUpdate
{
    internal class ChangeScriptFileNameComparer : IComparer<ChangeScriptFile>
    {
        public int Compare(ChangeScriptFile x, ChangeScriptFile y)
        {
            if (Equals(x, y))
            {
                return 0;
            }
            return StringComparer.OrdinalIgnoreCase.Compare(x.FileName, y.FileName);
        }
    }
}