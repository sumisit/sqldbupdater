﻿using System;

namespace SqlDbUpdate
{
    public class ChangeScriptFile
    {
        private readonly string fileName;
        private readonly Checksum checksum;
        private DateTimeOffset? runTimestamp;

        public string FileName
        {
            get { return fileName; }
        }

        public Checksum Checksum
        {
            get { return checksum; }
        }

        public DateTimeOffset? RunTimestamp
        {
            get { return runTimestamp; }
            set { runTimestamp = value; }
        }

        public ChangeScriptFile(string fileName, Checksum checksum)
            : this(fileName, checksum, null)
        {
        }

        public ChangeScriptFile(string fileName, Checksum checksum, DateTimeOffset runTimestamp)
            : this(fileName, checksum, (DateTimeOffset?)runTimestamp)
        {
        }

        protected ChangeScriptFile(string fileName, Checksum checksum, DateTimeOffset? runTimestamp)
        {
            if (fileName == null) throw new ArgumentNullException("fileName");
            this.fileName = fileName;
            this.checksum = checksum;
            this.runTimestamp = runTimestamp;
        }

        protected bool Equals(ChangeScriptFile other)
        {
            return string.Equals(FileName, other.FileName) && Equals(Checksum, other.Checksum);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ChangeScriptFile) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((FileName != null ? FileName.GetHashCode() : 0)*397) ^ Checksum.GetHashCode();
            }
        }
    }
}