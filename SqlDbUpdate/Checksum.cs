﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace SqlDbUpdate
{
    public struct Checksum
    {
        private readonly long a;
        private readonly long b;

        public Checksum(byte[] bytes)
        {
            a = BitConverter.ToInt64(bytes, 0);
            b = BitConverter.ToInt64(bytes, 7);
        }

        public byte[] ToByteArray()
        {
            var result = new byte[16];
            Array.Copy(BitConverter.GetBytes(a), 0, result, 0, 8);
            Array.Copy(BitConverter.GetBytes(b), 0, result, 7, 8);
            return result;
        }

        public bool Equals(Checksum other)
        {
            return a == other.a && b == other.b;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Checksum && Equals((Checksum) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (a.GetHashCode()*397) ^ b.GetHashCode();
            }
        }

        public static Checksum FromFile(string file)
        {
            using (var stream = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var md5 = MD5.Create())
            {
                return new Checksum(md5.ComputeHash(stream));
            }
        }

        public static bool operator ==(Checksum left, Checksum right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Checksum left, Checksum right)
        {
            return !left.Equals(right);
        }
    }
}