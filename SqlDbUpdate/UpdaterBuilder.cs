﻿using System;
using System.Data.SqlClient;

namespace SqlDbUpdate
{
	public class UpdaterBuilder
	{
		private string folder;
		private string connectionString;


		public UpdaterBuilder(string connectionString)
		{
			if (string.IsNullOrEmpty(connectionString))
				throw new ArgumentNullException("connectionString");

			
			this.connectionString = connectionString;
		}


		public UpdaterBuilder WithFolder(string folder)
		{
			this.folder = folder;
			return this;
		}

		public Updater Build()
		{
			return new Updater(connectionString, folder);
		}
	}
}