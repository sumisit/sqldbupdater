﻿using System;
using NDesk.Options.Extensions;
using SqlDbUpdate;
using System.Data.SqlClient;

namespace SqlDBUpdate
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			//log4net.Config.BasicConfigurator.Configure();
			//var hierarchy = ((log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository());
			//hierarchy.Root.Level = Level.Info;
			//hierarchy.RaiseConfigurationChanged(EventArgs.Empty);

			var os = new RequiredValuesOptionSet();

			Variable<string> database = os.AddRequiredVariable<string>("d|database", "The database name.");
			Variable<string> server = os.AddVariable<string>("S|server", "The server host name.");
			Switch trustedConnection = os.AddSwitch("E", "Use trused connection.");
			Variable<string> userName = os.AddVariable<string>("U", "Username.");
			Variable<string> password = os.AddVariable<string>("P", "Password.");
			Variable<string> folder = os.AddVariable<string>("f|folder", "The folder containing all SQL files.");

			var cm = new ConsoleManager("SQL Database Updater", os);

			if (!cm.TryParseOrShowHelp(Console.Out, args))
			{
				return;
			}


			#region Build connection string from parameters
			var connectionStringBuilder = new SqlConnectionStringBuilder();
			connectionStringBuilder.DataSource = string.IsNullOrEmpty(server) ? "(local)" : server;
			connectionStringBuilder.InitialCatalog = database;

			if (trustedConnection)
			{
				connectionStringBuilder.IntegratedSecurity = true;
			}
			else
			{
				connectionStringBuilder.UserID = userName;
				connectionStringBuilder.Password = password;
			}
			string connectionString = connectionStringBuilder.ConnectionString;

			#endregion

			UpdaterBuilder builder = new UpdaterBuilder(connectionString)
				.WithFolder(folder);

			Updater updater = builder.Build();
			updater.Execute();

#if DEBUG
			Console.ReadKey();
#endif
		}
	}
}